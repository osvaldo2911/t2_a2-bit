import java.util.Scanner;

public class Prueba {
	public static void main(String[] args) {
		Scanner e = new Scanner(System.in);
		OperacionesRecursividad or = new OperacionesRecursividad();
		System.out.println("ELIGA LA OPCI�N QUE DESEE: "
				+ "\n1)Divisores de Un N�mero"
				+ "\n2)Factorial"
				+ "\n3)Cocientes de Dos N�meros"
				+ "\n4)Cantidad de Vocales"
				+ "\n5)Cadena Inversa");
		
		byte opc = e.nextByte();
		switch (opc) {
		case 1:
			System.out.println("Ingrese el N�mero:");
			byte num = e.nextByte();
			System.out.println("Divisores: " + or.division(num, 2));
			break;
		case 2: 
			System.out.println("Ingrese el N�mero:");
			byte num2 = e.nextByte();
			System.out.println("N�meros Factoriales: " + or.factorial(num2)); 
			break;
		case 3: 
			System.out.println("Ingrese el Dividiendo:");
			byte dividiendo = e.nextByte();
			System.out.println("Ingrese el Divisor:");
			byte divisor = e.nextByte();
			System.out.println("Divisor entre los dos n�meros:" + or.divisores_2_nums(dividiendo, divisor));
			break;
		case 4:
			System.out.println("Ingrese una palabra o cadena:");
			String cadena = e.next();
			System.out.println("Cantidad de Vocales: " + or.vocales(cadena));
			break;
		case 5:
			System.out.println("Ingrese una palabra o cadena:");
			String pal = e.next();
			System.out.println("Cadena Inversa: " + or.invertirCadena(pal, pal.length()-1));
			break;
		default: System.out.println("No est� en la Lista.");
			break;
		}
		e.close();
	}
}
