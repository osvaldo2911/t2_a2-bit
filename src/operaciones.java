class OperacionesRecursividad{
	
   public int division(int num1, int num2) { //1
        if(num2==0)
            return num1;
        else
            return division(num2, num1 % num2);
    }
   
	public int factorial (int num) { //2
		if (num == 1) {
			return 1;
		}else {
			return (num*factorial(num-1));
		}
	}
	
	public int divisores_2_nums(int dividiendo, int divisor){
		if (dividiendo < divisor) {
			return 0;
		}else {
			return 1 + divisores_2_nums(dividiendo- divisor, divisor);
		}
	}

	
	public int vocales(String cadena) {
		if (cadena.length() == 0) {
			return 0;
		}
		switch (Character.toLowerCase(cadena.charAt(cadena.length() - 1))) {
			case 'a':
				return 1 + vocales(cadena.substring(0, cadena.length() - 1));
			case 'e':
				return 1 + vocales(cadena.substring(0, cadena.length() - 1));
			case 'i':
				return 1 + vocales(cadena.substring(0, cadena.length() - 1));
			case 'o':
				return 1 + vocales(cadena.substring(0, cadena.length() - 1));
			case 'u':
				return 1 + vocales(cadena.substring(0, cadena.length() - 1));
		}
		return 0 + vocales(cadena.substring(0, cadena.length() - 1));
	}
	
	public String invertirCadena(String palabra, int tam){ 
        if(tam==0){
            return palabra.charAt(tam)+"";
        }else{
            return palabra.charAt(tam) + (invertirCadena(palabra, tam-1));
        }
    }
}
public class operaciones {

}
